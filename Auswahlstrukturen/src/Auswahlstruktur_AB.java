import java.util.Scanner;
import java.util.InputMismatchException;



public class Auswahlstruktur_AB {

	
	public static void main(String[] Args) {
		
		boolean success = false;
		while(!success) {
			try {
				Noten();
				success = true;
			}
			catch(IllegalArgumentException e) {
				System.out.println("Die Eingabe ist ung�ltig, versuchen sie es bitte erneut.");
			}
			catch(InputMismatchException e) {
				System.out.println("Die Eingabe ist ung�ltig, versuchen sie es bitte erneut.");
				}
		}

		success = false;
		while(!success) {
			try {
				Schaltjahrberechner();
				success = true;
			}
			catch(IllegalArgumentException e) {
				System.out.println("Die Eingabe ist ung�ltig, versuchen sie es bitte erneut.");
			}
			catch(InputMismatchException e) {
			System.out.println("Die Eingabe ist ung�ltig, versuchen sie es bitte erneut.");
			}
		}
	}
	
	public static void Noten() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben sie ihre Note an.");
		
		int note = input.nextInt();
		
		switch(note) {
			case 1: {
				System.out.println("Sehr gut!");
				break;
			}
			case 2: {
				System.out.println("Gut");
				break;
			}
			case 3: {
				System.out.println("Befriedigend");
				break;
			}
			case 4: {
				System.out.println("Ausreichend");
				break;
			}
			case 5: {
				System.out.println("Mangelhaft");
				break;
			}
			case 6: {
				System.out.println("Ungen�gend");
				break;
			}
			default:
				throw new IllegalArgumentException();
		}
	}
	
	
	public static void Schaltjahrberechner() {
		Scanner input = new Scanner(System.in);
		int schaltjahr = input.nextInt();
		
		boolean istschaltjahr = Schaltjahr(schaltjahr);
		ausgabe(istschaltjahr, schaltjahr);
	}
	
	public static boolean Schaltjahr(int schaltjahr) {
		boolean istschaltjahr = true;
		
		if (schaltjahr % 400 == 0) {
			istschaltjahr = true;
		}
		else if (schaltjahr % 100 == 0) {
			istschaltjahr = false;
		}
		else if (schaltjahr % 4 == 0) {
			istschaltjahr = true;
		}
		else if (schaltjahr % 4 != 0) {
			istschaltjahr = false;
		}
		else {
			throw new IllegalArgumentException();
		}
	return istschaltjahr;
	}
	
	public static void ausgabe(boolean istschaltjahr, int schaltjahr) {
		
		if (istschaltjahr == true) {
			System.out.println("Das Jahr " + schaltjahr + " ist ein Schaltjahr.");
		}
		else {
			System.out.println("Das Jahr " + schaltjahr + " ist kein Schaltjahr.");

		}
	}
	
	
	
	
	
	
	
	
	
	
}
