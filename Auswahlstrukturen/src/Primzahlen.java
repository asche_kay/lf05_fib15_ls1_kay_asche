import java.util.Scanner;



// Aufgabe 4.4 AB Aufgabe 7 

public class Primzahlen {
	public static void main(String[] Args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben sie das obere Limit der Zahlen an, die auf Primheit �berpr�ft werden sollen.");
		int upper_limit = input.nextInt();
		
		print_primes(upper_limit);
		
		input.close();
	}
	
	
	
	public static void print_primes(int upper_limit) {
		int i = 1;
		boolean is_prime;
		do {
			//�berpr�ft, ob die laufende Variable Primzahl ist, und falls ja, schreibt sie auf
			
			is_prime = check_prime(i);
			
			if (is_prime == true) {
				System.out.print(i + " ");
			}
			i++;
		}
		while(i <= upper_limit);	
	}
	
	public static boolean check_prime(int prime) {
		 
		 boolean is_prime = false;
		 
		 // �berpr�ft ob die Zahl teilbar ist durch Zahlen die h�chstens halb so gro� wie die H�lfte der zu testenden Zahl ist.
		 // Da der kleinste Faktor einer Primfaktorzerlegunng 2 ist, muss der gr��te Faktor kleiner als die H�lfte der Zahl sein.
		 // prime + 2, wegen der m�glichen Abrundung durch die Interaktion von Integern und Division.
		 
		 for (int j = 2; j <= (prime+2)/2; j++) {
			 
			 if (prime == 2) {
				 is_prime = true;
				 break;
			 }
			 else if (prime % j == 0) {
				 is_prime = false;
				 break;
			 }
			 else {
				 is_prime = true;
			 } 
		 }
	return is_prime;
	}
	
	
}
