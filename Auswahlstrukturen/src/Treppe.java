import java.util.Scanner;


//Aufgabe 4.4 AB Aufgabe 9

public class Treppe {

	public static void main(String[] Args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben sie die H�he der Treppe ein");
		int hoehe = input.nextInt();
		System.out.println("Bitte geben sie die Stufenbreite der Treppe ein");
		int stufenbreite = input.nextInt();
		
		treppe(hoehe, stufenbreite);
		
		input.close();
	}
	
	public static void treppe(int hoehe, int stufenbreite) {
		
		for (int i = 1; i <= hoehe;  i++) {
			// Verschiebe die Sterne nach links
			for (int k = 0; k <= (hoehe-i)*stufenbreite; k++){
				System.out.print(" ");
			}
			// Erstelle die Treppe
			for (int j = 1; j <= i*stufenbreite; j++) {
					System.out.print("*");		
				}
				System.out.println("");
		}
	}
}
