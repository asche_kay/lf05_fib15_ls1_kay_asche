

//Aufgabe 4.4 AB Aufgabe 9


public class Wettlauf {

	public static void main(String[] args) {
		
		String ergebnis = wettlauf();
		System.out.println(ergebnis);
		
	}

	public static String wettlauf() {
		
		// intiialisiert Meteranzahl von L�ufer1/2, sowie Zeit		
		
		 double startlaufer1 = 0;
		 double startlaufer2 = 250;
		 double time = 0;
		 String ergebnis = "";
		 
		 System.out.printf("%-4s %s %-4s  %s  %-4s \n", "time" , " |" , "startlaufer1" , "|" , "startlaufer2");
		 System.out.println("-------------------------------------");

		 while (startlaufer1 <= 1000 & startlaufer2 <= 1000) {
			 
			 // definiert Funktionen f�r die Meteranzahl der L�ufer in abh�ngigkeit von der Zeit
			 // erh�ht die Zeit in jeder Durchf�hrung der Schleife
			 
			 time = time + 1;
			 startlaufer1 = startlaufer1 + 9.5*time;
			 startlaufer2 = startlaufer2 + 7*time;
			 System.out.printf("%-2.0f s  %s %-6.1f m %6s  %-6.1f m \n", time , "|" , startlaufer1 , "|" , startlaufer2);
		 }
		 System.out.println("---------------FIN-------------------");

		 
		 System.out.print("\n \n");
		 
		 if (startlaufer1 > startlaufer2) {
			 ergebnis = "L�ufer 1 gewinnt";
		 }
		 else if (startlaufer1 < startlaufer2) {
			 ergebnis = "L�ufer 2 gewinnt";
		 }
	return ergebnis;
	}
}
