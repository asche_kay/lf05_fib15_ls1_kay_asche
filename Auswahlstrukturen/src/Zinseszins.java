import java.util.Scanner;


// Zus�tzliche Aufgabe:AB Aufgabe 5  

public class Zinseszins {
	public static void main(String[] Args) {
		
		Scanner input = new Scanner(System.in);
		
		// Inputs erfragen
		
		System.out.print("Laufzeit in Jahren) des Sparvertrages: ");
		int laufzeit = input.nextInt();
		
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double kapital = input.nextDouble();
		
		System.out.print("Zinssatz im Format X,Y: ");
		double zinssatz = input.nextDouble();

	
		zinsen(kapital, zinssatz, laufzeit);
		
		
	}
	
	
	
	
	public static void zinsen(double kapital, double zinssatz, int laufzeit) {
		
	double anfangskapital = kapital;
	
	// Zinsen berechnen mit der Standardformel, es geschieht exponentiel, da es Zinseszinsen gibt.
		
	for (int i = 1; i <= laufzeit; i++) {
		kapital = kapital*(1+zinssatz/100);
	}
	
	// Formatierte Outputs
	
	System.out.print("\n");
	System.out.printf("%-25s %-5.2f %s ", "Eingezahltes Kapital:", anfangskapital, "Euro");
	System.out.print("\n");
	System.out.printf("%-25s %-5.2f %s", "Ausgezahltes Kapital:", kapital, "Euro");
	
	}

}
