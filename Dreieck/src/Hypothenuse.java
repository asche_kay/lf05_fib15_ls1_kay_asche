
public class Hypothenuse {
	double kathete1;
	double kathete2;
	
	public Hypothenuse(double kathete1, double kathete2) {
		this.kathete1 = kathete1;
		this.kathete2 = kathete2;
	}
	
	public static double hypothenuse(double kathete1, double kathete2) {
		double hypothenuse = Math.sqrt(Quadrat.quadrat(kathete1) + Quadrat.quadrat(kathete2));		
		return hypothenuse;
	}
}
