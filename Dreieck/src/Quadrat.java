
public class Quadrat {
	double x;
	
	public Quadrat(double x) {
		this.x = x;
	}
	
	public static double quadrat(double x) {
		double y = Math.pow(x, 2);
		return y;		
	}
	
}
