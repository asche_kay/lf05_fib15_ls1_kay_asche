import java.util.Scanner;

public class Seitenberechnung {

	public static void main(String[] Args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben sie die L�nge der ersten Kathete des rechtwinkligen Dreiecks in CM an.");
		double kathete1 = input.nextDouble();
		
		System.out.println("Bitte geben sie die L�nge der zweiten Kathete des rechtwinkligen Dreiecks in CM an.");
		double kathete2 = input.nextDouble();

		double hypothenuse = Hypothenuse.hypothenuse(kathete1, kathete2);
		
		System.out.printf("%s %.4f %s ", "Die L�nge der Hypothenuse des rechtwinkligen Dreiecks betr�gt " , hypothenuse , " CM");
		
		input.close();
	}
}
