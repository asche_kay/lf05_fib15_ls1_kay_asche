﻿import java.util.Scanner;


// Kommentar zu Aufgabe 2.4 
// Es werden mit den Variablen folgende Operationen durchgeführt:
// zuZahlenderBetrag(double): '=' einfache Zuweisung, '*' Produkt, '-' Differenz, 
// eingezahlterGesamtbetrag(double): '=' einfache Zuweisung, '<' Vergleich, "+" Addition, "-" Differenz
// eingeworfene Münze(double): "=" einfache Zuweisung, "+" Addition, 
// rückgabebetrag(double): "=" einfache Zuweisung, ">" & ">=" Vergleiche, "-=" 	Subtraktionszuweisung,  
// ticketnumber(double): "=" einfache Zuweisung, "*" Multiplikation

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double ticketnumber;
       
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       
       // Fragt Kunden nach der Gesamtanzahl der Tickets
       System.out.println("Wollen sie mehrere Tickets bestellen?");
       System.out.println("Bitte geben sie die Gesamtanzahl der Tickets ein:");
       ticketnumber = tastatur.nextDouble();
       
       //Multipliziert die Grundkosten mit der Anzahl der Tickets
       zuZahlenderBetrag = zuZahlenderBetrag * ticketnumber;  

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.00;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   //print zu print f geändert %.
    	   System.out.printf("%s %.2f \n", "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
}

// Kommentar Aufgabe 2.4 5,6:

// Alle bereits gegebenen Variablen sind definiert als Type:'double'
// Die neue Variable 'ticketnumber' ist ebenfalls als 'double' definiert, um eventuelle Typenumwandlungen zu vermeiden (obwohl es nicht zwingend notwendig ist, da es sich
// immer um einen Integer handelt und daher die Besorgnisse bzgl. der Genauigkeit nicht gerechtfertigt sind)
// double wird generell für alle Variablen verwendet, um die Maschinenpräzision zu maximieren und den natürlich auftretenden Rechenfehler so gering wie möglich zu halten.

// 6. Falls ticketnumber als integer definiert wurde: 
// Der Compiler wandelt zuerst den typ integer zu double um, und multipliziert anschließend die zwei Nummern vom Type 'double': 'zuzahlenderBetrag' und 'ticketnumber' 
// 	  Falls ticketnumber als double definiert wurde:
// Der Compiler multipliziert die beiden Nummern vom type 'double' ohne eine Typumwandlung durchzuführen.











