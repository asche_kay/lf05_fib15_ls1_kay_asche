import java.util.Scanner;


class Endlosfahrkartenautomat_String
{
	
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       boolean weiter = true;
       
       while(weiter == true) {
    	   
    	
	       //Erfasst Kosten & Anzahl der Tickets
	       zuZahlenderBetrag = fahrkartenbestellungErfassen();
	      
	       // Geldeinwurf mit verschiedenen M�nzen  
	       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       // Fahrkarte drucken
	       fahrkartenAusgeben(150);
	       
	       // R�ckgeldberechnung und -Ausgabe    
	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	       boolean success = false;
	       while (!success) {
	    	   try {
	    		   weiter = MehrFahrkarten();
	    		   System.out.println("");
	    		   success = true;
	    	   }
	    	   catch(IllegalArgumentException e) {
	    	   }
	       }
       }
	      
	       
	    
 	   
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir w�nschen Ihnen eine gute Fahrt.");
    } 
       
    
    
    public static double fahrkartenbestellungErfassen() {
    	
    	Scanner input1 = new Scanner(System.in);
    	
        System.out.print("Zu zahlender Betrag (EURO): ");
             
    	double zuZahlenderBetrag = input1.nextDouble();
    	
        System.out.println("Bitte geben sie die Gesamtanzahl der Tickets ein:");
    	int ticketnumber = input1.nextInt();
    	
    	if (ticketnumber <= 10 & ticketnumber > 0) {
    		    	}
    	else {
    		ticketnumber = 1;
    		System.out.println("Die Anzahl des Tickets muss sich zwischen 1 und 10 befinden. \nDie Ticketanzahl wurde als Default auf 1 gesetzt. \n \n");
    		System.out.println("Die Ticketzanzahl betr�gt " + ticketnumber+ " Tickets.");
    	}
    	
    	zuZahlenderBetrag = zuZahlenderBetrag * ticketnumber;
    	
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
        Scanner input = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0;
        double eingeworfeneM�nze;	
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("%s %.2f \n", "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = input.nextDouble();
     	   eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneM�nze;
        }   
        
        return eingezahlterGesamtbetrag;
    }
    
    
    public static void warte(int millisekunde) {
    	for (int i = 0; i < 15; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
    	
    	
    }
    
    public static void fahrkartenAusgeben(int millisekunde) {
    	
        System.out.println("\nFahrschein wird ausgegeben");
        
        warte(millisekunde);  
        
        System.out.println("\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double r�ckgabebetrag;
    	    	
    	r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("%s %.2f %s", "Der R�ckgabebetrag in H�he von " , r�ckgabebetrag , " EURO ");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	 muenzeAusgeben(2, "EURO"); 
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	 muenzeAusgeben(1, "EURO"); 
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	 muenzeAusgeben(50, "CENT"); 
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
           	 muenzeAusgeben(20, "CENT"); 
           	 r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
           	 muenzeAusgeben(10, "CENT"); 
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
           	 muenzeAusgeben(5, "CENT"); 
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
    
    public static void muenzeAusgeben(int wert, String einheit) {
    	System.out.printf("%d %s \n", wert , einheit);  	
    }
    
 
// Beginn der Aufgabe: Endloser Fahrkartenautomat

	public static boolean MehrFahrkarten() {
		boolean success = false;
		boolean weiter = false;
		Scanner input = new Scanner(System.in);
		String mehrkarten;
		
		
		System.out.println("M�chten sie noch weitere Fahrkarten kaufen? [ja/nein]");
		
		while (!success) {
			try {
				mehrkarten = input.next();
				weiter = fahrkartencheck(mehrkarten);
				success = true;
				}
			catch(IllegalArgumentException e) {
				System.out.println("Fehlerhafte Eingabe: Bitte geben sie [j/n] ein.");
			}
		}
	return weiter;
	}

	public static boolean fahrkartencheck(String mehrkarten) {
		boolean weiter = false;
				
		if (mehrkarten.equals("ja")) {
			weiter = true;
		}
		else if (mehrkarten.equals("nein")) {
			weiter = false;
		}
		else {
			throw new IllegalArgumentException();
		}
		
	return weiter;
	}
	
}






// reference types vs value types:
// value type = stores actual value
// reference types = stores reference to memory location of the actual variable
// Also: wenn String von System.in kommt -> anstatt == .equals() benutzen



