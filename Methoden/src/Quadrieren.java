import java.util.Scanner;

public class Quadrieren {
  
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bitte geben sie die zu quadrierende Zahl ein.");
		double x = input.nextDouble();
		System.out.println("Dieses Programm berechnet die Quadratzahl x� und rundet sie auf 2 Stellen nach dem Komma");
		System.out.println("---------------------------------------------");
		double k = Quadrieren(x);
		System.out.printf("x = %.2f und x�= %.2f\n", x, k);
		System.out.println("---------------------------------------------");
		System.out.println("Geben sie den Radius des Kreises an:");
		double y = input.nextDouble();
		System.out.println("Dieses Progamm wird nun den Fl�cheninhalt des Kreises mit Radius " + y + "berechnen");
		System.out.println("---------------------------------------------");
		double t = Kreisberechnung(y);
		System.out.printf("y = %.4f und A = %.4f \n", y , t);
	}	
	
	public static double Quadrieren(double x)
	{
		double ergebnis = x*x;
		
		return ergebnis;
	}
	
	
	public static double Kreisberechnung(double y) {
		double k = Quadrieren(y);
		double kreisfi = Math.PI * k;
		
		return kreisfi;
		
		
	}
	
	
}
