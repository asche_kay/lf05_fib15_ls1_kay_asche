public class Ausgabe01 {
	
	public static void main(String[] args) {

		
		System.out.println("Das ist ein Beispielsatz." + "\n" + "Ein Beispielsatz ist das.");
		System.out.println("Testtest");
		//println adds a linefeed after displaying the text whereas print does not
				
		
		System.out.printf("%20s * \n", " ");
		System.out.printf("%19s *** \n", " ");
		System.out.printf("%18s ***** \n", " ");
		System.out.printf("%17s ******* \n", " ");
		System.out.printf("%16s ********* \n", " ");
		System.out.printf("%15s *********** \n", " ");
		System.out.printf("%14s ************* \n", " ");
		System.out.printf("%19s *** \n", " ");
		System.out.printf("%19s *** \n \n", " ");
	    
		double zahl1 = 22.4234234;
		System.out.printf( "%.2f \n" , zahl1);
		double zahl2 = 11.22222;
		System.out.printf("%.2f \n" , zahl2);
		double zahl3 = 4.0;
		System.out.printf("%.2f \n" , zahl3);
		double zahl4 = 1000000.5521;
		System.out.printf("%.2f \n" , zahl4);
		double zahl5 = 97.34;
		System.out.printf("%.2f \n" , zahl5);
	
		
		System.out.printf("\n %2s ** \n", " ");
		System.out.printf(" * %4s * \n", " ");
		System.out.printf(" * %4s * \n", " ");		
		System.out.printf(" %2s ** \n", " ");		
		
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "0!","=" , " ","=" ,"1");
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "1!","=" , "1 ","=" , "1"  );
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "2!","=" , "1 * 2 ","=" , "2"  );
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "3!","=" , "1 * 2 * 3 ","=" , "6"  );
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "4!","=" , "1 * 2 * 3 * 4 ","=" , "24"  );
		System.out.printf( "%-5s %s %-19s %s %-4s \n", "5!","=" , "1 * 2 * 3 * 4 * 5 ","=" , "120"  );

		System.out.printf("%-12s %s %10s \n", "Fahrenheit", "|", "Celsius");
		System.out.println("-------------------------");
		System.out.printf("%+-12d %s %10.2f \n", -20, "|", -28.89);
		System.out.printf("%+-12d %s %10.2f \n", -10, "|", -23.33);		
		System.out.printf("%+-12d %s %10.2f \n", 0 , "|", -17.78);
		System.out.printf("%+-12d %s %10.2f \n", 20, "|", -6.67);
		System.out.printf("%+-12d %s %10.2f \n", 30, "|", -1.11);
		
		
		
		


	}

	
}
