
public class Ausfuehrung {

	public static void main(String[] Args) {
		
		
		//Erstelle Raumschiff & Ladung Objekte mit Parametern
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hergh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "N'Var", 5);
		
		Ladung klingonenSchwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmaWaffe = new Ladung("Plasma Waffe", 50);
		
		Ladung fierengie = new Ladung("Fierengie Schneckensaft", 200);
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung ("Rote Materie", 2);
		Ladung forschungsSonde = new Ladung("Forschungssonde", 35);
		Ladung photonenTorpedos = new Ladung("Photonentorpedo", 3);
		
		//Bef�lle Raumschiffe mit Ladung
		
		klingonen.setLadung(klingonenSchwert);
		klingonen.setLadung(fierengie);
		romulaner.setLadung(plasmaWaffe);
		romulaner.setLadung(roteMaterie);
		romulaner.setLadung(borgSchrott);
		vulkanier.setLadung(forschungsSonde);
		vulkanier.setLadung(photonenTorpedos);
		
		
		//Befehle aus der Aufgabenstellung
		
		
		//Klingonen schie�en auf Romulaner mit Photonentorpedo
		klingonen.photonentorpedoSchiessen(romulaner);
		//Romulaner schie�en zur�ck mit Phaserkanone
		romulaner.phaserkanoneSchiessen(klingonen);
		//Vulkanier schicken Nachricht "Gewalt ist nicht logisch."
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch.");
		
		//Klingonen geben Status & Ladungsverzeichnus wieder
		klingonen.attributeWiedergeben();
		klingonen.ladungsverzeichnisAusgeben();
		
		//Vulkanier reparieren ihr Schiff, Bef�llen ihre Torpedorohre und r�umen das Ladeverzeichnis auf
		vulkanier.reparaturAndroidenEinsetzen(5, true, true, true);
		vulkanier.photonenTorpedosEinsetzen(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		
		//Klingonen schie�en (vergeblich) Zwei weitere Torpedos auf die Romulaner
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);

		
		//Alle Raumschiffe zeigen ihren Status, sowie Ladeverzeichnis an
	
		klingonen.attributeWiedergeben();
		klingonen.ladungsverzeichnisAusgeben();
		
		romulaner.attributeWiedergeben();
		klingonen.ladungsverzeichnisAusgeben();
		
		vulkanier.attributeWiedergeben();
		vulkanier.ladungsverzeichnisAusgeben();
		
		//Der BroadcastKommunikator wird wiedergegeben. (Dieser ist bei jedem ja gleich.)
		vulkanier.eintraegeLogbuchZurueckgeben();
		
	}
}

