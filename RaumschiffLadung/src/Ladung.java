public class Ladung {

	private String bezeichnung;
	private int menge;
		
	public Ladung() {
	}
	
	public Ladung(String bezeichnung, int menge) {
		
		this.bezeichnung = bezeichnung;
		this.menge = menge;		
	}
	
	//public get/set Methoden
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	public void setBezeichnung(String neuBezeichnung) {
		this.bezeichnung = neuBezeichnung;
	}
	
	public int getMenge() {
		return this.menge;
	}
	
	public void setMenge(int neuMenge) {
		this.menge = neuMenge;
	}
	
	public String toString() {
		
		//Überschreibt die automatische toString Methode von Java.
			
		return "Die Bezeichnung der Ladung ist ''" + this.getBezeichnung() + "'' \n" 
				+ "Die Menge der Ladung beträgt: " + this.getMenge() ;
	}
	
}
