import java.util.*;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	
	public Raumschiff() {
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent,
			int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {
		
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
	}
	
	//Public get/set Methoden
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int neuPhotonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = neuPhotonentorpedoAnzahl;
	}
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	public void setEnergieversorgungInProzenz(int neuEnergieversorgungInProzent) {
		this.energieversorgungInProzent = neuEnergieversorgungInProzent;
	}
		
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
		
	public void setSchildeInProzenz(int neuSchildeInProzent) {
		this.schildeInProzent = neuSchildeInProzent;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	public void setHuelleInProzenz(int neuHuelleInProzent) {
		this.huelleInProzent = neuHuelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	public void setLebenserhaltungssystemeInProzenz(int neuLebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = neuLebenserhaltungssystemeInProzent;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void setAndroidenAnzahl(int neuAndroidenAnzahl) {
		this.androidenAnzahl = neuAndroidenAnzahl;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	public void setSchiffsname(String neuSchiffsname) {
		this.schiffsname = neuSchiffsname;
	}
	
	public void setLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);		
	}
	
	public void photonentorpedoSchiessen(Raumschiff zielraumschiff) {
		//�berpr�ft ob Photonentorpedos vorhanden sind, falls ja: feuert einen Torpedo auf das Ziel, falls nein: "Click"
		
		if(this.getPhotonentorpedoAnzahl() <= 0) {
			nachrichtAnAlle("-=*Click*=-");			
		}
		else {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() - 1);;
			nachrichtAnAlle("Photonentorpedo abgeschossen!");
			this.treffer(zielraumschiff);
		}	
	}
	
	public void phaserkanoneSchiessen(Raumschiff zielRaumschiff) {
		
		//�berpr�ft ob ausreichend Energie vorhanden ist um zu feuern und feuert, falls ja. (ansonsten: "Click")
		if(this.getEnergieversorgungInProzent() < 50)
			this.nachrichtAnAlle("-=Click=-");
		else {
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			this.setEnergieversorgungInProzenz(this.getEnergieversorgungInProzent() - 50);
			this.treffer(zielRaumschiff);
			
		}
	}
	
	public void trefferVermerken() {
		
		//If-Statements damit zuerst Schild -> H�lle -> Lebenserhaltungssysteme verringert werden
		
		nachrichtAnAlle(this.getSchiffsname() + " wurde getroffen!");
	}
		
	public void treffer(Raumschiff zielraumschiff) {
		zielraumschiff.trefferVermerken();
		
		if(zielraumschiff.getSchildeInProzent() >= 50) {
			zielraumschiff.setSchildeInProzenz(zielraumschiff.getSchildeInProzent() - 50);
		}
		else {
			zielraumschiff.setSchildeInProzenz(0);
			if(zielraumschiff.getHuelleInProzent() > 50) {			
				zielraumschiff.setHuelleInProzenz(zielraumschiff.getHuelleInProzent() - 50);
				zielraumschiff.setEnergieversorgungInProzenz(zielraumschiff.getEnergieversorgungInProzent() - 50);
			}
			else {
				zielraumschiff.setLebenserhaltungssystemeInProzenz(0);		
				nachrichtAnAlle("Lebenserhaltungssysteme von " + zielraumschiff.getSchiffsname() + " vernichtet");
			}		
		}		
	}
	
	public void nachrichtAnAlle(String Nachricht) {
		
		//F�gt Nachricht zum Broadcastkommunikator hinzu.
		
		broadcastKommunikator.add(Nachricht);
	}
	
	public void eintraegeLogbuchZurueckgeben() {
		
		//Gibt alle Eintr�ge des Broadcastkommunikators zur�ck.
		
		System.out.println("Gebe BroadcastKommunikator wieder...");		
		for (int index =0; index < broadcastKommunikator.size(); index++) {
			System.out.println("["+ broadcastKommunikator.get(index) + "]");
		}
	}
	
	public void attributeWiedergeben() {
		
		System.out.println("Gebe Status des Raumschiffs wieder...");
		System.out.println("======BRRRRT======");
		System.out.println("Der Name des Raumschiffs lautet: " + this.getSchiffsname() );
		System.out.println("Die Anzahl des Photonentorpedos lautet: " + this.getPhotonentorpedoAnzahl() );
		System.out.println("Die H�he der Energieversorgung lautet: " + this.getEnergieversorgungInProzent() );
		System.out.println("Die H�he der Energie der Schilde lautet: " + this.getSchildeInProzent());
		System.out.println("Die H�he der Integrit�t der H�lle lautet: " + this.getHuelleInProzent());
		System.out.println("Die H�he der Energie der Lebenserhaltungssysteme lautet: " + this.getLebenserhaltungssystemeInProzent() );
		System.out.println();
	}	
	
	public void ladungsverzeichnisAusgeben() {
		
		//Gibt alle Eintr�ge des Ladungsverzeichnisses zur�ck.
		
		System.out.println("Gebe das Ladeverzeichnis von: " + this.getSchiffsname() + " aus...");
		for(int index = 0; index < this.ladungsverzeichnis.size(); index++) {
			System.out.println(this.ladungsverzeichnis.get(index));
		}	
		System.out.println("");
	}
	
	public void photonenTorpedosEinsetzen(int anzahlNachladen) {
		
		//�berpr�ft ob eine Ladung mit dem Namen Photonentorpedo existiert, falls ja, l�dt so viele Torpedos nach wie m�glich (via if-statement)
		//Falls nein, macht "Click"
		
		boolean isPhotonenTorpedoLadungHere = false;
		
		for (int index = 0; index < this.ladungsverzeichnis.size(); index++) {
			if(this.ladungsverzeichnis.get(index).getBezeichnung().equals("Photonentorpedo")) {
				isPhotonenTorpedoLadungHere = true;
				if(this.ladungsverzeichnis.get(index).getMenge() < anzahlNachladen){
					this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + this.ladungsverzeichnis.get(index).getMenge());
					this.ladungsverzeichnis.get(index).setMenge(0);
					
					break;
				}
				else {
					this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlNachladen);
					this.ladungsverzeichnis.get(index).setMenge(this.ladungsverzeichnis.get(index).getMenge() - anzahlNachladen);
					break;
				}
			}
		}
		if(isPhotonenTorpedoLadungHere == false) {
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
		//�berpr�ft und l�scht alle Eintr�ge im Ladungsverzeichnus mit der Menge 0
		
		for (int index = 0; index < this.ladungsverzeichnis.size(); index++) {
			if(this.ladungsverzeichnis.get(index).getMenge() == 0) {
				this.ladungsverzeichnis.remove(index);	
			}
		}
	}
	
	public void reparaturAndroidenEinsetzen(int eingesetzteAndroiden, boolean repHuelle, boolean repSchilde, boolean repEnergieversorgung) {
		
		//Erstellt neue Zufallszahl, �berpr�ft welche Teile des Raumschiffs repariert werden m�ssen und berechnet dementsprechend die H�he der Reparatur
		//Die jeweiligen Raumschiffteile werden anschlie�end um diesen Wert erh�ht.
		
		Random rand = new Random();
		int randomInteger = rand.nextInt(100);
		
		if(eingesetzteAndroiden > this.getAndroidenAnzahl()) {
			eingesetzteAndroiden = this.getAndroidenAnzahl();
		}
		
		int anzahlZuReparieren = 0;
		
		if (repHuelle == true)
			anzahlZuReparieren++;
		if (repSchilde == true) 
			anzahlZuReparieren++;
		if (repEnergieversorgung == true)
			anzahlZuReparieren++;
				
		int anzahlRepariert = randomInteger * eingesetzteAndroiden / anzahlZuReparieren;
		
		if (repHuelle == true) 
			this.setHuelleInProzenz(this.getHuelleInProzent() + anzahlRepariert);
		if (repSchilde == true)
			this.setSchildeInProzenz(this.getSchildeInProzent() + anzahlRepariert);
		if (repEnergieversorgung == true)
			this.setEnergieversorgungInProzenz(this.getEnergieversorgungInProzent() + anzahlRepariert);
	}	
}	
		
		
		
		
	
	
	

