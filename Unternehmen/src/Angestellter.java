public class Angestellter{

	private String name;
	private double gehalt;

 
	public void setName(String name){
		this.name = name;
	}
	 public String getName() {
	 return this.name;
	 }
	 public void setGehalt(double gehalt) {
	 this.gehalt = gehalt;
	 }
	 public double getGehalt() {
	 return this.gehalt;
	 }
	 
	 
	 public static void main(String[] args)
	 {
	 Angestellter ang1 = new Angestellter();
	 Angestellter ang2 = new Angestellter();

	 ang1.setName("Meier");
	 ang1.setGehalt(4500);
	 ang2.setName("Petersen");
	 ang2.setGehalt(6000);
	 //Bildschirmausgaben
	 System.out.println("Name: " + ang1.getName());
	 System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
	 System.out.println("\nName: " + ang2.getName());
	 System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
	 

	 
	 }
}