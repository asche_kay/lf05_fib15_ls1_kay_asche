

public class artikel {

	
	private String name;
	private long artikelnr;
	private double einkaufspreis;
	private double verkaufspreis;
	private long sollbestand;
	private long istbestand;
	
	public artikel(String name, long artikelnr, double einkaufspreis, double verkaufspreis, long sollbestand, long istbestand) {
		
		this.name = name;
		this.artikelnr = artikelnr;
		this.einkaufspreis = einkaufspreis;
		this.verkaufspreis = verkaufspreis;
		this.sollbestand = sollbestand;
		this.istbestand = istbestand;
	}
	
	
	public String getName() {
		
		return this.name;
	}
	
	public long getArtikelnr() {
		return this.artikelnr;
	}
	
	public double getEinkaufspreis() {
		return this.einkaufspreis;
	}
	
	public double getVerkaufspreis() {
		return this.verkaufspreis;
	}
	
	public long getSollbestand() {
		return this.sollbestand;
	}
	
	public long getIstbestand() {
		return this.istbestand;
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setArtikelnr(long artikelnr) {
		this.artikelnr = artikelnr;
	}
	
	public void setEinkaufspreis(double einkaufspreis) {
		this.einkaufspreis = einkaufspreis;
	}
	
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	
	public void setSollbestand(long sollbestand) {
		this.sollbestand = sollbestand;
	}
	
	public void setIstbestand(long istbestand) {
		this.istbestand = istbestand;
	}
	
	public void nachbestellen() {
		long differenz = this.sollbestand - this.istbestand;
		long neubestellung = differenz + Math.round(0.5*this.sollbestand);
		
		
		if (differenz > 0) {
			this.setIstbestand(neubestellung);
			System.out.println("Es sind nicht genug " + this.name + " im Lager.");
			System.out.println("Es wurden " + neubestellung + " neue " + this.name + " gekauft.");

			double price = neubestellung * this.einkaufspreis;
			
			System.out.println("Die Neubestellung von " + neubestellung + " " + this.name + "'s hat "  + price + " Euro gekostet." );		
		}
		else {
			System.out.println("Es sind genug " + this.name + " im Lager vorhanden. Es wurde keine Bestellung ausgef�hrt.");
		}
		
	}

	public void istGenugimlager() {
		
		if (this.getIstbestand() <= 0.8 * this.getSollbestand()) {
			this.nachbestellen();
		}
	}
	
	public void artikelVerkaufen(long verkaufanzahl) {
		
		long neuIstbestand = this.istbestand - verkaufanzahl;
		if (neuIstbestand < 0) {
			System.out.println("Es sind nicht gen�gend " + this.name + " im Lager.");
			System.out.println("Vorgang abgebrochen.");
			System.exit(0);
		}
		else {
			this.setIstbestand(neuIstbestand);
			System.out.println("Es wurden " + verkaufanzahl + " " + this.name + "verkauft.");
		}
				
	}
	
	public void margeBerechnen(long verkaufsanzahl) {
		
		double marge = Math.round((verkaufsanzahl)*(this.verkaufspreis - this.einkaufspreis)*100)/100.0;
				
		System.out.println("Der gesamte Verkaufspreis betr�gt " + verkaufsanzahl*this.verkaufspreis + " Euro.");
		System.out.println("Der gesamte Einkaufspreis betr�gt " + verkaufsanzahl*this.einkaufspreis + " Euro.");
		System.out.println("Bei einem Verkauf von " + verkaufsanzahl + " " + this.name + "\nbetr�gt die Gewinnmarge " + marge + " Euro.");
			
	}	
}
