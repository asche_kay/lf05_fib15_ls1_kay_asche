package anmeldung_java;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
public class Verbindungstest {
public static void main(String[] args) {
try {
// Parameter für Verbindungsaufbau definieren
String driver = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost/versand";
String user = "root";
String password = "";
// JDBC-Treiber laden
Class.forName(driver);
// Verbindung aufbauen
Connection con;
con = DriverManager.getConnection(url, user, password);
// SQL-Anweisungen ausführen
Statement stmt = con.createStatement();
ResultSet rs = stmt.executeQuery("SELECT * FROM t_artikel");
// Ergebnis abfragen
while (rs.next()) {
System.out.println(rs.getString("bestand"));
}
// Verbindung schließen
con.close();
} catch (Exception ex) { // Fehler abfangen
ex.printStackTrace();// Fehlermeldung ausgeben
}
}
}

